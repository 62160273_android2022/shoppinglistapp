package com.techarat.myshoppinglist

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "grocery_items") data class GroceryItems (

    @NonNull @ColumnInfo(name = "itemName")
    var itemName: String,

    @NonNull @ColumnInfo(name = "itemQuantity")
    var itemQuantity: Int,

    @NonNull @ColumnInfo(name = "itemPrice")
    var itemPrice: Int,

    ){
    @PrimaryKey(autoGenerate = true)
    var id:Int? = null
}